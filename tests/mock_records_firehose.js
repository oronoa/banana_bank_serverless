const AWS = require('aws-sdk')

const kinesis = new AWS.Firehose({region:"us-east-1"})

for (var i=0; i<100;i++) {

  var params = {
    // Strings will be Base-64 encoded on your behalf 
    Record: { /* required */
      Data: `{"key":"${i}", "connections":"0" , "level": 0}` /* Strings will be Base-64 encoded on your behalf */ /* required */
    },
    //StreamName: "banana-main-stream-dev", 
    DeliveryStreamName: "banana-firehose", 
  }

  kinesis.putRecord(params, (err, data) => {
    if (err) console.log(err, err.stack) // an error occurred
    else     console.log(data)           // successful response
  })
}