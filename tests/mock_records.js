const AWS = require('aws-sdk')

const kinesis = new AWS.Kinesis({region:"us-east-1"})

const params = {
    // Strings will be Base-64 encoded on your behalf 
    Data: '{"key":"2", "connections":"0" , "level": 0}', 
    PartitionKey: '14ffdfddfdf545', 
    //StreamName: "banana-main-stream-dev", 
    StreamName: "banana-main-stream-dev", 
}

kinesis.putRecord(params, (err, data) => {
  if (err) console.log(err, err.stack) // an error occurred
  else     console.log(data)           // successful response
})