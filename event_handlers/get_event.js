
const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const kinesis = new AWS.Kinesis()
const dynamoDb = new AWS.DynamoDB.DocumentClient()

/**
 * This is our AWS Lambda event handler
 */
function process(event, context, callback) {
    event.Records.forEach(record => {
        const jsonStr = Buffer.from(record.kinesis.data, 'base64').toString()
        console.log(jsonStr)
        var o = JSON.parse(jsonStr)
        saveToDB(o)
        publishEvent(o)
    })
}

module.exports.process = process

// ========================================================================
// Private functions
// ========================================================================

function saveToDB(value) {
	const params = {
		TableName: 'accounts-dev',
		Item: {
			id: value.key,
            what: 'GOT_SOMETHING',
            more: 'AWS Rules!',
            price : 1,
			timestamp: new Date().toString()
		},
	}
	// Write the record to the database
	dynamoDb.put(params, err => {
		if(err) console.error(err, err.stack)
	})
}

function publishEvent(value) { 
	const key = value.key.toString(16)
	const params = {
		Data: JSON.stringify({
            type: 'SOMETHING_ELSE_HAPPENED',
			key: key,
			more_info: 707
        }),
		PartitionKey: key,
		StreamName: 'banana-main-stream-dev',
	}
	// Write record to stream
	kinesis.putRecord(params, err => {
		if(err) console.error(err, err.stack)
	})
}