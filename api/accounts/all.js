'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');
const R = require('ramda');

AWS.config.setPromisesDependency(require('bluebird'));

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.submit = (event, context, callback) => {
    console.log("Receieved request submit transaction details. Event is", event);
    const requestBody = JSON.parse(event.body);
    const createdOn = requestBody.createdOn;
    const desc = requestBody.desc;
    const ref = requestBody.ref;
    const credit = requestBody.credit;

    if (typeof createdOn !== 'string' || 
        typeof desc !== 'string' || 
        typeof credit !== 'number' || 
        typeof ref !== 'string') {
        console.error('Validation Failed');
        callback(new Error('Couldn\'t submit candidate because of validation errors.'));
        return;
    }

    const transaction = transactionInfo(createdOn, desc, credit, ref);
    const makeTransactionFx = R.composeP(submitCandidateEmailP, submitCandidateP, checkCandidateExistsP);

    candidateSubmissionFx(candidate)
        .then(res => {
            console.log(`Successfully submitted ${fullname}(${email}) candidate to system`);
            callback(null, successResponseBuilder(
                JSON.stringify({
                    message: `Sucessfully submitted candidate with email ${email}`,
                    candidateId: res.id
                }))
            );
        })
        .catch(err => {
            console.error('Failed to submit candidate to system', err);
            callback(null, failureResponseBuilder(
                409,
                JSON.stringify({
                    message: `Unable to submit candidate with email ${email}`
                })
            ))
        });
};


module.exports.list = (event, context, callback) => {
    console.log("Receieved request to list all transactions. Event is", event);
    var params = {
        TableName: process.env.ACCOUNTS_TABLE,
        ProjectionExpression: "id, createdOn, desc, ref, credit"
    };
    const onScan = (err, data) => {
        if (err) {
            console.log('Scan failed to load data. Error JSON:', JSON.stringify(err, null, 2));
            callback(err);
        } else {
            console.log("Scan succeeded.");
            return callback(null, successResponseBuilder(JSON.stringify({
                transactions: data.Items
            })
            ));
        }
    };
    dynamoDb.scan(params, onScan);
};

module.exports.get = (event, context, callback) => {
    const params = {
        TableName: process.env.CANDIDATE_TABLE,
        Key: {
            id: event.pathParameters.id,
        },
    };
    dynamoDb.get(params)
        .promise()
        .then(result => {
            callback(null, successResponseBuilder(JSON.stringify(result.Item)));
        })
        .catch(error => {
            console.error(error);
            callback(new Error('Couldn\'t fetch candidate.'));
            return;
        });
};

const checkCandidateExistsP = (candidate) => {
    console.log('Checking if candidate already exists...');
    const query = {
        TableName: process.env.CANDIDATE_EMAIL_TABLE,
        Key: {
            "email": candidate.email
        }
    };
    return dynamoDb.get(query)
        .promise()
        .then(res => {
            if (R.not(R.isEmpty(res))) {
                return Promise.reject(new Error('Candidate already exists with email ' + email));
            }
            return candidate;
        });
}

const submitCandidateP = candidate => {
    console.log('submitCandidateP() Submitting candidate to system');
    const candidateItem = {
        TableName: process.env.CANDIDATE_TABLE,
        Item: candidate,
    };
    return dynamoDb.put(candidateItem)
        .promise()
        .then(res => candidate);
};


const successResponseBuilder = (body) => {
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: body
    };
};

const failureResponseBuilder = (statusCode, body) => {
    return {
        statusCode: statusCode,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: body
    };
};


const submitCandidateEmailP = candidate => {
    console.log('Submitting candidate email');
    const candidateEmailInfo = {
        TableName: process.env.CANDIDATE_EMAIL_TABLE,
        Item: {
            candidate_id: candidate.id,
            email: candidate.email
        },
    };
    return dynamoDb.put(candidateEmailInfo)
        .promise();
}

const transactionInfo = (createdOn, desc, credit, ref) => {
    const timestamp = new Date().getTime();
    return {
        id: uuid.v1(),
        createdOn : createdOn,
        desc:desc,
        credit:credit,
        ref:ref,
        cleared: false,
        submittedAt: timestamp,
        updatedAt: timestamp,
    };
};