'use strict';

// ref
// https://serverless.com/framework/docs/providers/aws/events/cognito-user-pool/
// and 
// https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-lambda-pre-sign-up.html

module.exports.handler = (event, context, callback) => {
    // Set the user pool autoConfirmUser flag after validating the email domain
    console.log(event);
    event.response.autoConfirmUser = false;

    // Split the email address so we can compare domains
    var secret = event.request.userAttributes.secret_code;
    
    if (secret == 'poalim_rules') {
        event.response.autoConfirmUser = true;
    }
    
    // Return to Amazon Cognito
    callback(null, event);
};